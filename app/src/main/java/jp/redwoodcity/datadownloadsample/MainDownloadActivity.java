package jp.redwoodcity.datadownloadsample;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainDownloadActivity extends Activity implements OnClickListener {
    static String TAG = "Download1";
    String downloadUrl = "http://webyacojp.sakura.ne.jp/app/sagasu/tantei_s2_starnation/manifest.json";
    EditText urlEditText;
    Button startButton;
    TextView progressTextView;
    ImageView imageView;
    DownloadProgressBroadcastReceiver progressReceiver;
    IntentFilter intentFilter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();
        setListeners();
        registerDownloadBroadcastReceiver();
        startDownload();
    }

    protected void findViews() {
        urlEditText = (EditText) findViewById(R.id.urlEditText);
        startButton = (Button) findViewById(R.id.startButton);
        progressTextView = (TextView) findViewById(R.id.progressTextView);
        imageView = (ImageView) findViewById(R.id.imageView1);
    }

    protected void setListeners() {
        startButton.setOnClickListener(this);
    }

    protected void registerDownloadBroadcastReceiver() {
        progressReceiver = new DownloadProgressBroadcastReceiver();
        intentFilter = new IntentFilter();
        intentFilter.addAction("DOWNLOAD_PROGRESS_ACTION");
        registerReceiver(progressReceiver, intentFilter);
    }

    protected void startDownload() {
        Intent intent = new Intent(getBaseContext(), DownloadService.class);
//        intent.putExtra("url", urlEditText.getText().toString());
        intent.putExtra("url", downloadUrl);
        startService(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.startButton:
                startDownload();
                break;
        }
    }

    class DownloadProgressBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Show Progress
            Bundle bundle = intent.getExtras();
            int completePercent = bundle.getInt("completePercent");
            int totalByte = bundle.getInt("totalByte");
            String progressString = totalByte + " byte read.";
            if (0 < completePercent) {
                progressString += "[" + completePercent + "%]";
            }
            progressTextView.setText(progressString);

            String fileName = bundle.getString("filename");

//            // If completed, show the picture.
//            if (completePercent == 100) {
//                String fileName = bundle.getString("filename");
//                Bitmap bitmap = BitmapFactory
//                        .decodeFile("/data/data/jp.redwoodcity.datadownloadsample/files/"
//                                + fileName);
//                if (bitmap != null) {
//                    imageView.setImageBitmap(bitmap);
//                }
//            }

            FileInputStream input;
            try {
                input = openFileInput(fileName);
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuffer strBuffer = new StringBuffer();
                String line;
                while ((line = reader.readLine()) != null) {
                    strBuffer.append(line);
                }
                // ストリームを閉じる
                reader.close();

//                JSONObject json = null;
                try {
                    JSONObject json = new JSONObject(strBuffer.toString());
//                    Log.d(TAG, json.toString());
                    // タイトルを取得
                    String gameTitle = json.getString("gameTitle");
                    String gameUrlFlow = json.getString("gameUrlFlow");
                    String gameUrlItem = json.getString("gameUrlItem");
                    String timeLimit = json.getString("timeLimit");
                    String questionNum = json.getString("questionNum");

                    Log.d(TAG, gameTitle);
                    Log.d(TAG, gameUrlFlow);
                    Log.d(TAG, gameUrlItem);
                    Log.d(TAG, timeLimit);
                    Log.d(TAG, questionNum);

                    for (int i = 0; i < Integer.parseInt(questionNum); i++) {

                        String questiontool = json.getString("question" + (i + 1) + "tool");
                        Log.d(TAG, questiontool);
                        if(json.has("question" + (i + 1) + "answerText")) {
                            String questionanswerText = json.getString("question" + (i + 1) + "answerText");
                            Log.d(TAG, questionanswerText);
                        }
                        if(json.has("question" + (i + 1) + "answerMapLat")) {
                            String questionanswerMapLat = json.getString("question" + (i + 1) + "answerMapLat");
                            Log.d(TAG, questionanswerMapLat);
                        }
                        if(json.has("question" + (i + 1) + "answerMapLon")) {
                            String questionanswerMapLat = json.getString("question" + (i + 1) + "answerMapLon");
                            Log.d(TAG, questionanswerMapLat);
                        }
                        String questionanswerincorrect = json.getString("question" + (i + 1) + "answerincorrect");
                        Log.d(TAG, questionanswerincorrect);
                        String questionanswernextbutton = json.getString("question" + (i + 1) + "answernextbutton");
                        Log.d(TAG, questionanswernextbutton);
                        String questionmapZoom = json.getString("question" + (i + 1) + "mapZoom");
                        Log.d(TAG, questionmapZoom);
                        String questionmapLat = json.getString("question" + (i + 1) + "mapLat");
                        Log.d(TAG, questionmapLat);
                        String questionmapLon = json.getString("question" + (i + 1) + "mapLon");
                        Log.d(TAG, questionmapLon);

                    }

                    progressTextView.setText(json.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e) {
                // TODO 自動生成された catch ブロック
                e.printStackTrace();
            } catch (IOException e) {
                // TODO 自動生成された catch ブロック
                e.printStackTrace();
            }
        }
    }
}